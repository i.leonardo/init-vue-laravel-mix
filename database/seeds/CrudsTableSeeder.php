<?php

use App\Models\Cruds;
use Illuminate\Database\Seeder;

class CrudsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cruds::create([
            'name' => 'Teste',
            'color' => 'green',
        ]);
    }
}
