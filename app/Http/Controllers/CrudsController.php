<?php

namespace App\Http\Controllers;

use App\Models\Cruds;
use Faker\Generator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CrudsController extends Controller
{
    /**
     * Lista os dados do DB
     *
     * @return Response
     */
    public function index()
    {
        return response(Cruds::all(), Response::HTTP_OK);
    }

    /**
     * Cria um novo dado
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
        ]);
        $data = Cruds::create($request->all());

        return response($data, Response::HTTP_CREATED);
    }

    /**
     * Cria um novo dado Fake
     *
     * @param  Generator $faker
     * @return Response
     */
    public function create(Generator $faker)
    {
        $crud = new Cruds();
        $crud->name = $faker->lexify('????????');
        $crud->color = $faker->boolean ? 'red' : 'green';
        $crud->save();

        return response($crud, Response::HTTP_CREATED);
    }

    /**
     * Atualiza o dado específico
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
        ]);
        $data = Cruds::findOrFail($id);
        $data->update($request->all());

        return response($data, Response::HTTP_OK);
    }

    /**
     * Deleta o dado específico
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $crud = Cruds::findOrFail($id);
        $crud->delete();

        return response(null, Response::HTTP_OK);
    }
}
