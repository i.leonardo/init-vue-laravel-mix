# Vue + Laravel Mix 

> **API** com consulta via **axios** para o **vue** construir os dados
>
> Use **PHP 7.2.15**

CRUD simples para demonstrar o uso do front e back end juntos.

![Demonstração](https://gitlab.com/i.leonardo/init-vue-laravel-mix/raw/master/main.png)

## Requisitos

> - [Laravel](https://laravel.com/): **v5.8.*** - Framework PHP
> - [Vue](https://vuejs.org/): **v2.6.10** - Framework JS
> - **Dependências:**
>   - [axios](https://github.com/axios/axios): **v0.18.0** - Cliente HTTP com promisse
>   - [boostrap](https://getbootstrap.com/): **v4.3.1** - Framework CSS
> - **Editor usado:**
> - Visual Studio Code
>     - [editorconfig.editorconfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig): **EditorConfig for VS Code**
>     - [kokororin.vscode-phpfmt](https://marketplace.visualstudio.com/items?itemName=kokororin.vscode-phpfmt): **PHP Formatter**
>     - [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint): **ESLint** - [Deixe o ESLint trabalhar para você, no Visual Studio Code](http://vuejs-brasil.com.br/deixe-o-eslint-trabalhar-para-voce-no-visual-studio-code/)

## Project Setup

```bash
# install dependencies
composer install
npm install

# Update Class
composer dump-autoload

# DB
php artisan migrate --seed

# Compiles and hot-reloads for development (port 8000)
npm run watch
php artisan serve
```

## Árvore (tree)

```
.
|   .editorconfig
|   .env (Config)
|   .gitignore
|   artisan (Artisan CLI)
|   composer.json
|   LICENSE
|   package.json
|   phpunit.xml
|   README.md
|   webpack.mix.js (configura o 'public')
|
+---app
|   +---Http
|   |   \---Controllers
|   |
|   \---Models
|
+---database
+---public
+---resources
|   +---assets (front-end VueJS)
|   \---views
|
\---routes
```

## Licença

> [MIT License](/blob/master/LICENSE)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)

