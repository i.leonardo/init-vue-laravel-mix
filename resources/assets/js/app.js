// Imports
import Vue from 'vue';
import App from './App.vue';
import './bootstrap';

// Config
Vue.config.devtools = process.env.NODE_ENV === 'production' ? false : true;

// Init
new Vue({
    render: h => h(App),
}).$mount('#app');
